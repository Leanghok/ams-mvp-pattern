//
//  API.swift
//  AMS
//
//  Created by Hour Leanghok on 12/17/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation

struct API {
    static let header = [
        "Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=",
        "Accept": "application/json",
        "Content-Type": "application/json"
    ]
    static var basedAPIURL: String{
        return "http://www.api-ams.me/v1/api/"
    }
    
    static var POSTArticle: URL {
        return URL(string: API.basedAPIURL + "articles")!
    }
    
    static func ARTICLE_LIST(page: Int) -> URL {
        return URL(string: API.basedAPIURL + "articles?page=\(page)&limit=15")!
    }
    
    static func ARTICLE_UPDATE(id: Int) -> URL{
        return URL(string: API.basedAPIURL + "articles/\(id)")!
    }
    
    static func ARTICLE_DELETE(id: Int) -> URL {
        return URL(string: API.basedAPIURL + "articles/\(id)")!
    }
    
    static var UPLOAD_FILE: URL {
        return URL(string: API.basedAPIURL + "uploadfile/single")!
    }
}
