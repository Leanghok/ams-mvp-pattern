//
//  ArticleService.swift
//  AMS MVP Pattern
//
//  Created by Hour Leanghok on 12/24/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

enum CompletionHandler {
    case success(String)
    case failure(String)
}

protocol ArticleServiceProtocol {
    func didResponseArticle(article: [Article])
    func didResponseMessage(message: String)
    func didUploadArticle()
}


class ArticleService{
    
    var delegate: ArticleServiceProtocol?
    
    //TODO: Get Articles from API
    func getArticles(page:Int){
        var request = URLRequest(url: API.ARTICLE_LIST(page: page))
        request.httpMethod = HTTPMethod.get.rawValue
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard error == nil else {
                print("Error")
                return
            }
            
            guard let data = data else {
                print("Data is null")
                return
            }
            
            do {
                let result = try JSONDecoder().decode(JSONArticleResponse.self, from: data)
                self.delegate?.didResponseArticle(article: result.articles!)
            }catch let err{
                print(err)
            }
            
            }.resume()
    }
    
    //TODO: Upload Image to API
    func uploadImage(url: URL,image: UIImage ,handleComplete: @escaping ((String) -> Void)) {
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(image.jpegData(compressionQuality: 0.2)!, withName: "FILE", fileName: ".jpg", mimeType: "image/jpeg")
        }, to: url, method: .post, headers: API.header) { (result) in
            switch result{
            case .success(request: let upload, _ , _):
                upload.responseData(completionHandler: { (response) in
                    let data = try? JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any]
                    //                    imageData = data!["DATA"] as? String
                    handleComplete(data!["DATA"] as! String)
                })
            case .failure(let error):
                print(error)
            }
        }
        return
    }
    
    //TO DO: Post Article to API
    func uploadArticle(title:String, description: String, image: UIImage, completionHander: @escaping (CompletionHandler) -> Void){
        uploadImage(url: API.UPLOAD_FILE, image: image) { (imageUrl) in
            let params: Parameters = [
                "TITLE": title,
                "DESCRIPTION": description,
                "IMAGE": imageUrl
            ]
            Alamofire.request(API.POSTArticle, method: .post, parameters: params, encoding: JSONEncoding.default, headers: API.header).responseData { (response) in
                if response.result.isSuccess {
                    self.delegate?.didUploadArticle()
                    completionHander(.success("Done"))
                }else{
                    completionHander(.failure("Error"))
                }
            }
        }
    }
    
    //TO DO: Update Article with API
    func updateArticle(id: Int, title: String, description: String, image: UIImage?, completionHandler: @escaping (CompletionHandler) -> Void){

        let header = ["Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=", "Content-Type": "application/json", "Accept": "application/json"]
        
        var params:Parameters = [:]
        print("Updated Article Run")
        if image == nil {
            print("Updated Article image is nil")
            params = [
                "TITLE": title,
                "DESCRIPTION": description
            ]
        }else{
            self.uploadImage(url: API.UPLOAD_FILE, image: image!) { (imageUrl) in
                params = [
                    "TITLE": title,
                    "DESCRIPTION": description,
                    "IMAGE": imageUrl
                ]
            }
        }
        Alamofire.request(API.ARTICLE_UPDATE(id: id), method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if response.result.isSuccess {
                completionHandler(.success("Updated"))
            }else{
                print("Updated Article Run \(response.description)")
            }
        }
    }
    
    //TO DO: Delete Article with API
    func deleteArticle(id: Int, completionHandler: @escaping (CompletionHandler) -> Void){
        Alamofire.request(API.ARTICLE_DELETE(id: id), method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if response.result.isSuccess{
                completionHandler(.success("Deleted"))
            }else{
                completionHandler(.failure("Can't be deleted"))
            }
        }
    }
    
}
