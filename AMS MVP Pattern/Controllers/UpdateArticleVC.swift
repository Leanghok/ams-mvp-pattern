//
//  UpdateArticleVC.swift
//  AMS
//
//  Created by Hour Leanghok on 12/22/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit
import Kingfisher
class UpdateArticleVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: Local Variable
    var articleService: ArticleService?
    var articleID = 0
    var articleTitle = ""
    var descrption = ""
    var imageUrl:URL?
    var imagePicker = UIImagePickerController()
    var didSelectImage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Initialization
        articleService = ArticleService()
        
        titleTextField.text = articleTitle
        descriptionTextView.text = descrption
        if let url = imageUrl{
            fetchAndShowImage(url: url, imageView: imageView)
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    @IBAction func saveButtonTap(_ sender: UIButton) {
        if didSelectImage{
            self.articleService?.updateArticle(id: articleID, title: titleTextField.text!, description: descriptionTextView.text, image: imageView.image, completionHandler: { (response) in
                _ = self.navigationController?.popViewController(animated: true)
            })
        }else{
            self.articleService?.updateArticle(id: articleID, title: titleTextField.text!, description: descriptionTextView.text, image: nil, completionHandler: { (response) in
                _ = self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    func fetchAndShowImage(url: URL, imageView: UIImageView){
        var imageView = imageView
        imageView.kf.setImage(with: url)
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "no_image_available"),
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
    
}
extension UpdateArticleVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
        })
        imageView.image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        didSelectImage = true
    }
}
