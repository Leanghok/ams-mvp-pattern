//
//  HomeVC.swift
//  AMS
//
//  Created by Hour Leanghok on 12/17/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit
import Kingfisher

class HomeVC: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var articleTableView: UITableView!
    
    //MARK: Local Variable
    var articlePresenter: ArticlePresenter?
    var articles:[Article]?
    let refreshControl = UIRefreshControl()
    var page = 1
    var currentArrayShouldBe = 15
    var isView:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TO DO: Initialization
        articlePresenter = ArticlePresenter()
        articles = [Article]()
        articlePresenter?.delegate = self
        isView = false
        
        //TO DO: Register Table View Custom Article Cell
        articleTableView.register(UINib(nibName: "ArticleCustomTableViewCell", bundle: nil), forCellReuseIdentifier: "ArticleCustomCell")
        
        //TO DO: Set refresh to TableView
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        articleTableView.addSubview(refreshControl)
        
        //TO DO: Fetch data from API
        self.fetchData(page: 1) {
            self.articleTableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isView!{
            isView = false
            self.articleTableView.reloadData()
        }else{
            self.articles?.removeAll()
            self.fetchData(page: 1) {
                self.articleTableView.reloadData()
            }
            self.currentArrayShouldBe = 15
            self.page = 1
            self.articleTableView.reloadData()
        }
        
    }
    @objc func refresh(refreshControl: UIRefreshControl) {
        articles?.removeAll(keepingCapacity: true)
        fetchData(page:1) {
            DispatchQueue.main.async {
                refreshControl.endRefreshing()
                self.currentArrayShouldBe = 15
                self.page = 1
            }
        }
    }
    
}

//TO DO: Conform TableView Protocols
extension HomeVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCustomCell") as! ArticleCustomTableViewCell
        cell.articleTitleLabel.text = articles![indexPath.row].title
        cell.articleDescriptionLabel.text = articles![indexPath.row].description

        //TO DO: Check imageUrl if nil or not a URL before set
        if let imageUrl = articles![indexPath.row].imageUrl {
            if let url = URL(string: imageUrl) {
                fetchAndShowImage(url: url, imageView: cell.articleImageView)
            }else{
                print("Not a url")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         print("currentArray \(currentArrayShouldBe)")
         print("currentArray indexPath\(indexPath.row)")
        if currentArrayShouldBe - indexPath.row <= 3 {
            page += 1
            self.currentArrayShouldBe += 15
            fetchData(page: page) {
                DispatchQueue.main.async {
                    self.articleTableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isView = true
        let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "viewArticleView") as! ViewArticleVC
        if let imageUrl = articles![indexPath.row].imageUrl{
            if let url = URL(string: imageUrl){
                if let data = try? Data(contentsOf: url) {
                    vc.articleImage = UIImage(data: data)
                }
            }
        }
        vc.articleTitle = articles![indexPath.row].title!
        vc.articleDescription = articles![indexPath.row].description!
        show(vc, sender: nil)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            self.articlePresenter?.deleteArticle(id: self.articles![indexPath.row].id, completionHandler: { (response) in
                switch response{
                case .success(let message):
                    print(message)
                case .failure(let message):
                    print(message)
                }
            })
            tableView.performBatchUpdates({
                self.articles?.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
                self.articleTableView.reloadData()
            }, completion: nil)


        }

        let edit = UITableViewRowAction(style: .default, title: "Edit") { (action, indexPath) in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "updateArticleView") as! UpdateArticleVC
            vc.articleTitle = self.articles![indexPath.row].title!
            vc.descrption = self.articles![indexPath.row].description!
            vc.articleID = self.articles![indexPath.row].id

            if let imageUrlString = self.articles![indexPath.row].imageUrl {
                if let imageUrl = URL(string: imageUrlString) {
                    vc.imageUrl = imageUrl
                }
            }
            self.show(vc, sender: nil)

        }

        edit.backgroundColor = UIColor.lightGray

        return [delete, edit]
        
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print("Row \(indexPath.row)")
    }
}

extension HomeVC{
    private func fetchData(page: Int, completionHandler: @escaping () -> Void){
        self.articlePresenter?.getArticles(page: page)
        if page == 1 {
            self.articleTableView.reloadData()
        }
        completionHandler()
    }
}

extension HomeVC: ArticlePresenterProtocol{
   
    func didUploadArticle() {
    }

    func didResponseArticles(article: [Article]) {
        self.articles = self.articles! + article
        DispatchQueue.main.async {
            self.articleTableView.reloadData()
        }
    }
    
    func didResponseMessage(message: String) {
        
    }
    
    
}

//Mark: Kingfisher library functions implementation
extension HomeVC{
    func fetchAndShowImage(url: URL, imageView: UIImageView){
        var imageView = imageView
        imageView.kf.setImage(with: url)
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "no_image_available"),
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
}

