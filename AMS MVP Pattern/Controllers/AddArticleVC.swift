//
//  AddArticleVC.swift
//  AMS
//
//  Created by Hour Leanghok on 12/21/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import UIKit

class AddArticleVC: UIViewController {

    //MARK: Outlet
    @IBOutlet weak var titleTextField: UITextField!

    @IBOutlet weak var descriptionTextView: UITextView!

    @IBOutlet weak var imageView: UIImageView!

    //MARK: Local Variable
    var articleService:ArticleService?
    var didSelectImage = false
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Initialization
        articleService = ArticleService()
        
        //Mark: Add tap gesture to ImageView
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false

            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    @IBAction func saveButtonTap(_ sender: UIButton) {

        var title = ""
        var image = UIImage(named: "no_image_available")

        if let titleText = titleTextField.text {
            title = titleText
        }

        if didSelectImage == true{
            image = imageView.image as! UIImage
        }


        self.articleService!.uploadArticle(title: title, description: descriptionTextView.text, image: image!) { (response) in
            switch response{
            case .success(let message):
                _ = self.navigationController?.popViewController(animated: true)
                print(message)
            case .failure(let message):
                print(message)
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension AddArticleVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
        })
        imageView.image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        didSelectImage = true
    }
}
