//
//  ArticlePresenter.swift
//  AMS MVP Pattern
//
//  Created by Hour Leanghok on 12/24/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation
import UIKit
protocol ArticlePresenterProtocol {
    func didResponseArticles(article: [Article])
    func didResponseMessage(message: String)
    func didUploadArticle()
}

class ArticlePresenter{
    var delegate: ArticlePresenterProtocol?
    var articleService: ArticleService?
    init(){
        self.articleService = ArticleService()
        self.articleService?.delegate = self
    }
    func getArticles(page: Int){
        self.articleService?.getArticles(page: page)
    }
    
    func uploadArticle(title:String, description: String, image: UIImage, completionHandler: @escaping (CompletionHandler) -> Void){
        self.articleService?.uploadArticle(title: title, description: description, image: image, completionHander: { (response) in
            switch response{
            case .success(let message):
                completionHandler(.success(message))
            case .failure(let message):
                completionHandler(.failure(message))
            }
        })
    }
    
    func updateArticle(id:Int, title:String, description: String, image: UIImage?, completionHandler: @escaping (CompletionHandler) -> Void){
        self.articleService?.updateArticle(id: id, title: title, description: description, image: image, completionHandler: { (response) in
            switch response {
            case .success(let message):
                completionHandler(.success(message))
            case .failure(let message):
                completionHandler(.failure(message))
            }
        })
    }
    
    func deleteArticle(id:Int, completionHandler: @escaping (CompletionHandler) -> Void){
        self.articleService?.deleteArticle(id: id
            , completionHandler: { (response) in
                switch response{
                case .success(let msg):
                    completionHandler(.success(msg))
                case .failure(let msg):
                    completionHandler(.failure(msg))
                }
        })
    }
    
}

extension ArticlePresenter: ArticleServiceProtocol{
    
    func didUploadArticle() {
        self.delegate?.didUploadArticle()
    }
    
    func didResponseArticle(article: [Article]) {
        self.delegate?.didResponseArticles(article: article)
    }
    
    func didResponseMessage(message: String) {
        self.delegate?.didResponseMessage(message: message)
    }
}
